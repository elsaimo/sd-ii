/*!
 * @author  [Mohamed Elsaid](melsaid@uncc.edu)
 * @version  V1.0
 * @date  12/08/2023
 */
#include <Wire.h>
#include <Adafruit_MLX90614.h> //temp sensor library
#include <SPI.h>             // f.k. for Arduino-1.5.2
#define USE_SDFAT
#include <SdFat.h>           // Use the SdFat library
SdFatSoftSpi<12, 11, 13> SD; //Bit-Bang on the Shield pins

#include <Adafruit_GFX.h>    // Hardware-specific library
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;

#include <TouchScreen.h>
#include <SoftWire.h> //to configer the any pin to be SDA and SCL 
#include <SoftwareSerial.h>
#include <arduinoFFT.h>

#define SD_CS     10
#define NAMEMATCH ""         // "" matches any name
//#define NAMEMATCH "tiger"    // *tiger*.bmp
#define PALETTEDEPTH   8     // support 256-colour Palette

char namebuf[32] = "/";   //BMP files in root directory
//char namebuf[32] = "/bitmaps/";  //BMP directory e.g. files in /bitmaps/*.bmp
Adafruit_MLX90614 mlx = Adafruit_MLX90614();


//accelerometer and solenoid registration 
const uint16_t SAMPLES = 128; //This value MUST ALWAYS be a power of 2   not enough memory
const double SAMPLING_FREQUENCY = 16000; //Hz, must be less than 10000 due to ADC

arduinoFFT FFT = arduinoFFT();

unsigned int sampling_period_us;
unsigned long microseconds;

double vReal[SAMPLES];
double vImag[SAMPLES];

const int accelerometerPin = A15;//connected the accelerometer control to any analog pin 
const int solenoidPin = 47; //connected the solenoid control to any digital pin 
//end 


File root;
int pathlen;

#define YP A3
#define XM A2
#define YM 9
#define XP 8

#define TS_MINX 150
#define TS_MINY 130
#define TS_MAXX 920
#define TS_MAXY 940


#define  BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define MINPRESSURE 10
#define MAXPRESSURE 1000

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

unsigned long lastTouchTime = 0; 
const int DEBOUNCE_DELAY = 200;   

int X;
int Y;

const int KEYPAD_COLS = 3;  // 3 columns
const int KEYPAD_ROWS = 4;  // 4 rows
const int BUTTON_WIDTH = 50;
const int BUTTON_HEIGHT = 50;
const int BUTTON_SPACING = 2;

char keys[KEYPAD_ROWS][KEYPAD_COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'C', '0', 'S'}  // 'C' for clear, 'S' for start
};

String inputText = "";  // To store the input
String D = "";
SoftWire sw(39, 40);  // Software I2C on pins 39 (SDA) and 40 (SCL)
const int sensorAddress = 0x57;


char setAddress[5] = {0xFA,0x04,0x01,0x80,0x81};
char measureCont[5] = {0x80,0x06,0x03,0x77};
char bin[4] = {};

String accumulatedData = ""; // Global variable to store accumulated data

int SD_test = 0; //0 means good , 1 means bad 

// Define the button states
#define BTN_INACTIVE 0
#define BTN_ACTIVE   1

// Button dimensions
#define BTN_WIDTH  160
#define BTN_HEIGHT 50
#define BTN_MARGIN 10   // Margin from the edge of the screen
#define BTN_SPACING 10  // Space between buttons
// Button placement
#define BTN_X1 80// Left button X. test mode
#define BTN_Y  40// Both buttons Y
#define BTN_Y_2 120
#define BTN_X2 80// Right button X


#define BTN_WIDTH_BACK  50
#define BTN_HEIGHT_BACK 50
#define BTN_MARGIN_BACK 10   // Margin from the edge of the screen
#define BTN_SPACING_BACK 10 

#define BTN_X3_BACK 10
#define BTN_Y3_BACK (180 + BTN_SPACING)
// Button text
#define BTN_TEXT1 "Temp."
#define BTN_TEXT2 "Freq."
#define BTN_TEXT3 "Start"
#define BTN_TEXT4 "Test"
#define BTN_TEXT5 "Back"
#define BTN_TEXT6 "D"

//char response[11];
void setup()
{
  uint16_t ID;
  sw.begin();
  Serial.begin(9600);
  
  //ADCSRA &= ~(bit (ADPS0) | bit (ADPS1) | bit (ADPS2)); //clear prescaler bits
  //ADCSRA |= bit (ADPS2);      //  16
  
  ADCSRA &= ~((1 << ADPS2) | (1 << ADPS0));  
  ADCSRA |= (1 << ADPS1);  

  pinMode(solenoidPin, OUTPUT);  // Set the solenoid pin as output
  digitalWrite(solenoidPin, LOW);  // Ensure solenoid is off initially
  sampling_period_us = round(1000000 * (1.0 / SAMPLING_FREQUENCY));

  mlx.begin();
  Wire.begin();
  Serial.println("\nI2C Scanner");
  tft.reset();  
  tft.begin(0x9341);  // My LCD uses LIL9341 Interface driver IC
  tft.setRotation(3);  

  Serial.print("Show BMP files on TFT with ID:0x");
  ID = tft.readID();
  Serial.println(ID, HEX);
  if (ID == 0x0D3D3) ID = 0x9481;
  tft.begin(ID);
  tft.setRotation(5);
  tft.fillScreen(WHITE);
  tft.setTextColor(0xFFFF, 0x0000);
  bool good = SD.begin(SD_CS);
  if (!good) {
      Serial.print(F("cannot start SD"));
      SD_test = 1;
  }
  
  root = SD.open(namebuf);
  Serial.print("root: ");
  Serial.println(root);
  pathlen = strlen(namebuf);
  
  if (SD_test == 0){
    IntroScreen();
  }
  SD_Scan ();
  Scan_temp_sensor();
  setting();
}

void SD_Scan (){
  if (SD_test == 1){
    tft.setCursor(10, 10);
    tft.fillScreen(BLACK);                  // Black Background
    tft.setTextColor(RED);                // Set Text Properties
    tft.setTextSize(1);
    tft.println(" Initialization failed. Things to check: \n");
    tft.println("* Is a card inserted?\n");
    delay(5000);
  }else if (SD_test == 0){
    tft.setCursor(10, 10);
    tft.fillScreen(BLACK);                  // Black Background
    tft.setTextColor(GREEN);                // Set Text Properties
    tft.setTextSize(1);
    tft.println(" SD Card initialization was successful. \n");
    if (SD.exists("data.csv")){
      tft.println(" data.csv has been detected. \n");
      SD.remove("data.csv");
      tft.println(" data.csv has been deleted. \n");
    }
    delay(5000);
  }
}
// Define a state for the display
enum ScreenState {
  SCREEN_KEYPAD,
  SCREEN_TEST_BUTTONS,
  SCREEN_SETTING_BUTTONS
};

ScreenState currentScreen = SCREEN_SETTING_BUTTONS; 

void loop() {

  TSPoint p = waitTouch();
  X = p.x;
  Y = p.y;

  unsigned long currentMillis = millis();
  
  if (currentMillis - lastTouchTime > DEBOUNCE_DELAY) {
    // Determine action based on current screen state
    switch (currentScreen) {
      case SCREEN_KEYPAD:
        handleKeypadTouch(p);
        break;
      case SCREEN_TEST_BUTTONS:
        handleButtonTouch(p);
        break;
      case SCREEN_SETTING_BUTTONS:
        handleButtonTouch2(p);
        break;
    }
    lastTouchTime = currentMillis;  // Update the last touch time
  }
}

void handleKeypadTouch(TSPoint p) {
  if (isButtonPressed(p, 270, 190, 50, 50)) {
    setting();
  } else if (isButtonPressed(p, 175, 190, 50, 50)){ 
    D = inputText ; 
    inputText = "";
    displayText();
  }

  char key = getKey(p.x, p.y);
  if (key) {
    if (key == 'C') {
      inputText = "";  // Clear the input
      displayText();
    } else if (key == 'S') {
      // Handle the "Start" action
      String id = inputText; //life data
      float temperature = Temp(); //life data
      float oil_Level = 65.23; //dummy data
      
      double frequency = measureFrequency(); 
      Serial.println(frequency);
      //int distance = readDistance();

      const char* status = "Normal";   //dummy data
      //const char* status = "BAD";   //dummy data

      tft.setCursor(10, 10);
      tft.fillScreen(BLACK);                  
      tft.setTextColor(WHITE);               
      tft.setTextSize(2);

      tft.print("Temperature: ");
      tft.print(temperature);
      tft.println(" F");
      
      tft.println(" ");
      tft.print(" Distance: ");
      tft.println(D);
      tft.println(" ");
      
      tft.print(" Frequency: ");
      tft.print(frequency);
      tft.println(" Hz");
      tft.println(" ");

      if (status == "BAD"){
        tft.setTextColor(RED);               
        tft.setTextSize(2);
        tft.print(" Status: ");
        tft.println(status);
      }else {
        tft.setTextColor(GREEN);               
        tft.setTextSize(2);
        tft.print(" Status: ");
        tft.println(status);
      }
      if (SD_test == 0){
        appendToCsv("data.csv",id, temperature, frequency, status , D);
      }
      delay (5000);
      tft.fillScreen(BLACK);
      drawKeypad();
      displayText();
    } else {
      inputText += key;  // Append the key to the input
    }
    displayText();
  }
}

void handleButtonTouch(TSPoint p) {
  if (isButtonPressed(p, BTN_X1, BTN_Y, BTN_WIDTH, BTN_HEIGHT)) {
    float temperature = Temp(); //life data
    tft.setCursor(10, 10);
    tft.fillScreen(BLACK);                  
    tft.setTextColor(WHITE);               
    tft.setTextSize(2);
    tft.print("Temperature: ");
    tft.print(temperature);
    tft.println(" F");
    delay(2000); // Wait for 2 seconds
    // Switch back to buttons screen after function call
    testStart();
  }else
  if (isButtonPressed(p, BTN_X2, BTN_Y_2, BTN_WIDTH, BTN_HEIGHT)) {
    double frequency = measureFrequency(); // Call soli function
    tft.setCursor(10, 10);
    tft.fillScreen(BLACK);                  
    tft.setTextColor(WHITE);               
    tft.setTextSize(2);
    tft.print(" Frequency: ");
    tft.print(frequency);
    tft.println(" Hz");
    delay(2000); // Wait for 2 seconds
    // Switch back to buttons screen after function call
    testStart();
  }else
  if (isButtonPressed(p, BTN_X3_BACK, BTN_Y3_BACK, BTN_WIDTH, BTN_HEIGHT)){
    setting();
  }
}


bool isButtonPressed(TSPoint p, int btnX, int btnY, int btnW, int btnH) {
  // Debug output to see the expected button boundaries
  unsigned long currentMillis = millis();
  if (currentMillis - lastTouchTime > DEBOUNCE_DELAY) {
    int px = p.x;
    int py = p.y;
    
    delay(1000);
    bool isPressed = (py >= btnX && py <= (btnX + btnW) && px >= btnY && px <= (btnY + btnH));
    lastTouchTime = currentMillis;  // Update the last touch time
    return isPressed;
  }
}

void testStart() {
  tft.fillScreen(BLACK);
  currentScreen = SCREEN_TEST_BUTTONS;
  drawButton(BTN_X1, BTN_Y, BTN_WIDTH, BTN_HEIGHT, BTN_TEXT1, BTN_INACTIVE);
  drawButton(BTN_X2, BTN_Y_2, BTN_WIDTH, BTN_HEIGHT, BTN_TEXT2, BTN_INACTIVE);
  drawButton_back (BTN_X3_BACK, BTN_Y3_BACK, BTN_WIDTH_BACK, BTN_HEIGHT_BACK, BTN_TEXT5, BTN_INACTIVE);
}

void setting() {
  tft.fillScreen(BLACK);
  currentScreen = SCREEN_SETTING_BUTTONS;
  drawButton(BTN_X1, BTN_Y, BTN_WIDTH, BTN_HEIGHT, BTN_TEXT3, BTN_INACTIVE);
  drawButton(BTN_X2, BTN_Y_2, BTN_WIDTH, BTN_HEIGHT, BTN_TEXT4, BTN_INACTIVE);
}

void handleButtonTouch2(TSPoint p) {
  if (isButtonPressed(p, BTN_X1, BTN_Y, BTN_WIDTH, BTN_HEIGHT)) {
    tft.fillScreen(BLACK);
    currentScreen = SCREEN_KEYPAD;
    drawKeypad();
    displayText();
    delay(2000); // Wait for 2 seconds
  }else
  //delay(2000);
  if (isButtonPressed(p, BTN_X2, BTN_Y_2, BTN_WIDTH, BTN_HEIGHT)) {
    currentScreen = SCREEN_TEST_BUTTONS;
    testStart();
    delay(2000); // Wait for 2 seconds    
  }
}


void drawButton(int x, int y, int w, int h, const char *label, bool state) {
    tft.fillRect(x, y, w, h, state ? TFT_LIGHTGREY : TFT_DARKGREY);
    tft.drawRect(x, y, w, h, TFT_WHITE);
    tft.setCursor(x + ((w - strlen(label) * 6)/2) - 9, y + h/2 - 9);
    tft.setTextColor(TFT_BLACK);
    tft.setTextSize(2);
    tft.print(label);
}

void drawButton_back(int x, int y, int w, int h, const char *label, bool state) {
    tft.fillRect(x, y, w, h, state ? TFT_LIGHTGREY : TFT_DARKGREY);
    tft.drawRect(x, y, w, h, TFT_WHITE);
    tft.setCursor(x + (w - strlen(label) * 6)/2, y + h/2 - 4);
    tft.setTextColor(TFT_BLACK);
    tft.setTextSize(1);
    tft.print(label);
}
void temp() {
    // Code for the temp button
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(50, 50);
    tft.setTextColor(TFT_WHITE);
    tft.setTextSize(2);
    tft.print("Temp function called");
}


void IntroScreen()
{
    showBMP("uncc2.bmp", 0, 0);
    delay(1000);
    showBMP("stabilus.bmp", 0, 0);
    tft.drawRoundRect(45, 240, 150, 20, 5, BLACK);
    tft.setCursor(52, 210);
    tft.setTextSize(2);
    tft.setTextColor(BLACK);
    tft.println("system Loading");
    delay(1000);
    for(int i=0; i<150; i+=10){
      tft.fillRoundRect(45, 240, 10+i, 20, 5,  BLACK);
      delay(100);
      }
    
    delay(5000);
    tft.fillScreen(WHITE);
}

#define BMPIMAGEOFFSET 54
#define BUFFPIXEL      20

uint16_t read16(File& f) {
    uint16_t result;         // read little-endian
    f.read(&result, sizeof(result));
    return result;
}

uint32_t read32(File& f) {
    uint32_t result;
    f.read(&result, sizeof(result));
    return result;
}

uint8_t showBMP(char *nm, int x, int y)
{
    File bmpFile;
    int bmpWidth, bmpHeight;    // W+H in pixels
    uint8_t bmpDepth;           // Bit depth (currently must be 24, 16, 8, 4, 1)
    uint32_t bmpImageoffset;    // Start of image data in file
    uint32_t rowSize;           // Not always = bmpWidth; may have padding
    uint8_t sdbuffer[3 * BUFFPIXEL];    // pixel in buffer (R+G+B per pixel)
    uint16_t lcdbuffer[(1 << PALETTEDEPTH) + BUFFPIXEL], *palette = NULL;
    uint8_t bitmask, bitshift;
    boolean flip = true;        // BMP is stored bottom-to-top
    int w, h, row, col, lcdbufsiz = (1 << PALETTEDEPTH) + BUFFPIXEL, buffidx;
    uint32_t pos;               // seek position
    boolean is565 = false;      //

    uint16_t bmpID;
    uint16_t n;                 // blocks read
    uint8_t ret;

    if ((x >= tft.width()) || (y >= tft.height()))
        return 1;               // off screen

    bmpFile = SD.open(nm);      // Parse BMP header
    bmpID = read16(bmpFile);    // BMP signature
    (void) read32(bmpFile);     // Read & ignore file size
    (void) read32(bmpFile);     // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile);       // Start of image data
    (void) read32(bmpFile);     // Read & ignore DIB header size
    bmpWidth = read32(bmpFile);
    bmpHeight = read32(bmpFile);
    n = read16(bmpFile);        // # planes -- must be '1'
    bmpDepth = read16(bmpFile); // bits per pixel
    pos = read32(bmpFile);      // format
    if (bmpID != 0x4D42) ret = 2; // bad ID
    else if (n != 1) ret = 3;   // too many planes
    else if (pos != 0 && pos != 3) ret = 4; // format: 0 = uncompressed, 3 = 565
    else if (bmpDepth < 16 && bmpDepth > PALETTEDEPTH) ret = 5; // palette 
    else {
        bool first = true;
        is565 = (pos == 3);               // ?already in 16-bit format
        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * bmpDepth / 8 + 3) & ~3;
        if (bmpHeight < 0) {              // If negative, image is in top-down order.
            bmpHeight = -bmpHeight;
            flip = false;
        }

        w = bmpWidth;
        h = bmpHeight;
        if ((x + w) >= tft.width())       // Crop area to be loaded
            w = tft.width() - x;
        if ((y + h) >= tft.height())      //
            h = tft.height() - y;

        if (bmpDepth <= PALETTEDEPTH) {   // these modes have separate palette
            //bmpFile.seek(BMPIMAGEOFFSET); //palette is always @ 54
            bmpFile.seek(bmpImageoffset - (4<<bmpDepth)); //54 for regular, diff for colorsimportant
            bitmask = 0xFF;
            if (bmpDepth < 8)
                bitmask >>= bmpDepth;
            bitshift = 8 - bmpDepth;
            n = 1 << bmpDepth;
            lcdbufsiz -= n;
            palette = lcdbuffer + lcdbufsiz;
            for (col = 0; col < n; col++) {
                pos = read32(bmpFile);    //map palette to 5-6-5
                palette[col] = ((pos & 0x0000F8) >> 3) | ((pos & 0x00FC00) >> 5) | ((pos & 0xF80000) >> 8);
            }
        }

        // Set TFT address window to clipped image bounds
        tft.setAddrWindow(x, y, x + w - 1, y + h - 1);
        for (row = 0; row < h; row++) { 
            uint8_t r, g, b, *sdptr;
            int lcdidx, lcdleft;
            if (flip)   // Bitmap is stored bottom-to-top order (normal BMP)
                pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
            else        // Bitmap is stored top-to-bottom
                pos = bmpImageoffset + row * rowSize;
            if (bmpFile.position() != pos) { // Need seek?
                bmpFile.seek(pos);
                buffidx = sizeof(sdbuffer); // Force buffer reload
            }

            for (col = 0; col < w; ) {  //pixels in row
                lcdleft = w - col;
                if (lcdleft > lcdbufsiz) lcdleft = lcdbufsiz;
                for (lcdidx = 0; lcdidx < lcdleft; lcdidx++) { // buffer at a time
                    uint16_t color;
                    // Time to read more pixel data?
                    if (buffidx >= sizeof(sdbuffer)) { // Indeed
                        bmpFile.read(sdbuffer, sizeof(sdbuffer));
                        buffidx = 0; // Set index to beginning
                        r = 0;
                    }
                    switch (bmpDepth) {          // Convert pixel from BMP to TFT format
                        case 24:
                            b = sdbuffer[buffidx++];
                            g = sdbuffer[buffidx++];
                            r = sdbuffer[buffidx++];
                            color = tft.color565(r, g, b);
                            break;
                        case 16:
                            b = sdbuffer[buffidx++];
                            r = sdbuffer[buffidx++];
                            if (is565)
                                color = (r << 8) | (b);
                            else
                                color = (r << 9) | ((b & 0xE0) << 1) | (b & 0x1F);
                            break;
                        case 1:
                        case 4:
                        case 8:
                            if (r == 0)
                                b = sdbuffer[buffidx++], r = 8;
                            color = palette[(b >> bitshift) & bitmask];
                            r -= bmpDepth;
                            b <<= bmpDepth;
                            break;
                    }
                    lcdbuffer[lcdidx] = color;

                }
                tft.pushColors(lcdbuffer, lcdidx, first);
                first = false;
                col += lcdidx;
            }           // end cols
        }               // end rows
        tft.setAddrWindow(0, 0, tft.width() - 1, tft.height() - 1); //restore full screen
        ret = 0;        // good render
    }
    bmpFile.close();
    return (ret);
}

void Scan_temp_sensor() {
  byte error, address;
  int nDevices;
  tft.setCursor(10, 10);
  tft.fillScreen(BLACK);                  // Black Background
  tft.setTextColor(WHITE);                // Set Text Properties
  tft.setTextSize(1);

  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      tft.print("I2C (Temperature sensor) device found at address 0x");
      if (address<16) {
        tft.print("0");
      }
      Serial.print(address,HEX);
      tft.println(" !");
      nDevices++;
    }
  }
  if (nDevices == 0) {
    tft.setTextColor(RED);
    tft.println("No I2C (Temperature sensor) devices found\n");
  } else {
    tft.setTextColor(GREEN);
    tft.println("done\n");
  }
  delay(5000); // Wait 5 seconds and scan again
  tft.fillScreen(BLACK); 
}

double Temp(){
  double temp = mlx.readObjectTempF();   // Read temperature in Celsius. Use 
  return temp;
}

String lastID = "";
int IDCount = 0;

void appendToCsv(const char* filename, String data4, double data1, float data2, const char* data3 , String data5) {
  SdFile file;

  if (data4 == lastID) {
      IDCount++; 
      data4 += "." + String(IDCount);  
  } else {
      IDCount = 0;
  }
  lastID = data4;

  if (!file.open(filename, O_WRITE | O_CREAT | O_AT_END)) {
    Serial.println("Opening file failed!");
    return;
  }

  file.println("ID,Temperature,Frequancy,Status, Distance");
  file.print(data4);
  file.print(",");
  file.print(data1, 4); 
  file.print(",");       
  file.print(data2, 4);  
  file.print(",");       // CSV delimiter
  file.println(data3);  // Print data3 with 4 decimal places and a newline
  file.print(data5);
  file.print(",");
  file.close();
}

TSPoint waitTouch() {
  TSPoint p;
  do {
    p = ts.getPoint();
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
  } while ((p.z < MINPRESSURE) || (p.z > MAXPRESSURE));
  p.x = map(p.x, TS_MINX, TS_MAXX, 0, 240);
  p.y = map(p.y, TS_MINY, TS_MAXY, 0, 320);
  return p;
}

void displayText() {
  tft.fillRect(5, 5, 320, 30, TFT_WHITE);  // Clear the text area 
  tft.setCursor(10, 15);
  tft.setTextColor(TFT_BLACK);  // Use a distinct color for visibility
  tft.setTextSize(2);
  tft.print(inputText);
}

void drawKeypad() {
  for (int r = 0; r < KEYPAD_ROWS; r++) {
    for (int c = 0; c < KEYPAD_COLS; c++) {
      int x = 5 + c * (BUTTON_WIDTH + BUTTON_SPACING);
      int y = 30 + r * (BUTTON_HEIGHT + BUTTON_SPACING);  // Adjusted for space for the text display
      tft.fillRect(x, y, BUTTON_WIDTH, BUTTON_HEIGHT, TFT_BLUE);
      tft.setCursor(x + BUTTON_WIDTH/3, y + BUTTON_HEIGHT/3);
      tft.setTextColor(TFT_WHITE);
      tft.setTextSize(2);
      if (keys[r][c] != ' ') {  // Don't draw space keys
        tft.print(keys[r][c]);
      }
    }
  }
  tft.fillRect(5, 5, 320, 30, TFT_WHITE);  // Clear the text area 
  tft.setCursor(175, 100);
  tft.setTextColor(TFT_WHITE);  // Use a distinct color for visibility
  tft.setTextSize(2);
  tft.println("C: Clear");
  tft.setCursor(175, 120);
  tft.print("S: Start");
  tft.setCursor(175, 80);
  tft.print("Instructions");
  tft.setCursor(175, 140); 
  tft.print("D: Distance");

  drawButton_back (175, 190, 50, 50, BTN_TEXT6, BTN_ACTIVE); 
  drawButton_back (270, 190, 50, 50, BTN_TEXT5, BTN_ACTIVE);

}

char getKey(int x, int y) {
  unsigned long currentMillis = millis();
  if (currentMillis - lastTouchTime > DEBOUNCE_DELAY) {
    int tempX = x;
    x = y;
    y = tempX;

    for (int r = 0; r < KEYPAD_ROWS; r++) {
      for (int c = 0; c < KEYPAD_COLS; c++) {
        int x1 = 5 + c * (BUTTON_WIDTH + BUTTON_SPACING);
        int y1 = 30 + r * (BUTTON_HEIGHT + BUTTON_SPACING);
        int x2 = x1 + BUTTON_WIDTH;
        int y2 = y1 + BUTTON_HEIGHT;
        
        if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
          return keys[r][c];
        }
      }
    }
    lastTouchTime = currentMillis;  // Update the last touch time
    return 0; // No key pressed
  }
}


void exciteDamper() {
  digitalWrite(solenoidPin, HIGH);  // Turn on the solenoid
  delay(50);  // Excite for 50ms. You can adjust this value.
  digitalWrite(solenoidPin, LOW);   // Turn off the solenoid
  delay(10);  // Wait a bit for the vibration to stabilize before measurement.  14ms
}

double measureFrequency() {
  exciteDamper();  // Excite the damper before measurement change to kHz
  /* Data Acquisition */
  microseconds = micros();
  for (int i = 0; i < SAMPLES; i++) {
    vReal[i] = analogRead(accelerometerPin);
    vImag[i] = 0;
    while (micros() - microseconds < sampling_period_us) {
      // Wait to match the desired sampling frequency
      //try that inside while parameter micros() < (microseconds + sampling_period_us)
    }
    microseconds += sampling_period_us;
  }

  /* Compute FFT */
  FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
  FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
  FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);
  double dominantFrequency = FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY);
  return dominantFrequency;  // Return the dominant frequency
}

